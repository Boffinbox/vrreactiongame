﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK.Controllables.ArtificialBased;

public class GameStateManager : MonoBehaviour
{
    enum GameState { Initializing, Playing, Scoring, Resting }
    static GameState gs = GameState.Resting;
    [SerializeField] GameObject buttonHolder;
    List<GameObject> buttonList = new List<GameObject>();
    GameObject currentButton;
    [SerializeField] Text timerText;
    [SerializeField] Text scoreText;
    [SerializeField] GameObject timerCanvas;
    [SerializeField] GameObject endCanvas;
    float timer;
    int score;
    void Update()
    {
        if (gs == GameState.Initializing)
        {
            InitializeGameState();
            gs = GameState.Playing;
        }
        if (gs == GameState.Playing)
        {
            PlayingLoop();
        }
        if (gs == GameState.Scoring)
        {
            timerCanvas.SetActive(false);
            endCanvas.SetActive(true);
            gs = GameState.Resting;
        }
        if (gs == GameState.Resting)
        {
            // foo
        }
    }
    void PlayingLoop()
    {
        if (currentButton == null)
        {
            ChooseNextButton();
        }
        else if (currentButton.GetComponentInChildren<VRTK_ArtificialPusher>().GetNormalizedValue() > 0.8f)
        {
            CorrectButtonPressed();
        }
        if (timer <= 0)
        {
            timer = 0;
            gs = GameState.Scoring;
        }
        else
        {
            timer -= Time.deltaTime;
        }
        timerText.text = timer.ToString("##.###");
        scoreText.text = score.ToString();
    }
    void CorrectButtonPressed()
    {
        score++;
        currentButton.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        ChooseNextButton();
    }
    private void InitializeGameState()
    {
        if (currentButton != null)
        {
            currentButton.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
            currentButton = null;
        }
        score = 0;
        timer = 60f;
        buttonList = new List<GameObject>();
        foreach (Transform a in buttonHolder.transform)
        {
            buttonList.Add(a.gameObject);
        }
        ChooseNextButton();
    }
    void ChooseNextButton()
    {
        int randomInt = Random.Range(0, buttonList.Count);
        if (buttonList[randomInt].GetComponentInChildren<VRTK_ArtificialPusher>().GetNormalizedValue() > 0f)
        {
            currentButton = null;
        }
        else
        {
            currentButton = buttonList[randomInt];
            currentButton.GetComponentInChildren<MeshRenderer>().material.color = new Color(0.961f, 0.475f, 0.225f); //new Color(245,121,58);
        }
    }
    public static void StartGame()
    {
        gs = GameState.Initializing;
    }
}
