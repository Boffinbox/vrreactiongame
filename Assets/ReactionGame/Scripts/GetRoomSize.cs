﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetRoomSize : MonoBehaviour
{
    /// <summary>
    /// The width and the length of the room as Vector3, where y=0
    /// </summary>
    public static Vector3 Size { get => size; }
    static Vector3 size;
    void Start()
    {
        StartCoroutine(SetBounds());
    }
    IEnumerator SetBounds()
    {
        Valve.VR.HmdQuad_t fourCorners = new Valve.VR.HmdQuad_t();
        // magic line from reddit, it checks to see if the bounds exist yet
        while (!SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref fourCorners))
        {
            yield return new WaitForSeconds(0.1f);
        }
        float width = Mathf.Abs(fourCorners.vCorners0.v0 - fourCorners.vCorners1.v0);
        float length = Mathf.Abs(fourCorners.vCorners2.v2 - fourCorners.vCorners1.v2);
        size = new Vector3(width, 0, length);
        yield return null;
    }
}