﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPanelPlacement : MonoBehaviour
{
    bool hasPlacedPanel;
    private void Update()
    {
        if (!hasPlacedPanel)
        {
            if (GetRoomSize.Size != null)
            {
                if (GetRoomSize.Size.z > 1.5f)
                {
                    transform.position = new Vector3(GetRoomSize.Size.x / 3f, 1.25f, 0f);
                }
                else
                {
                    transform.position = new Vector3(GetRoomSize.Size.x / 3f, (GetRoomSize.Size.z / 2f) + 0.5f, 0f);
                }

                hasPlacedPanel = true;
            }
        }
    }
}