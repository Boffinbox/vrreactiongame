﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPanelScaler : MonoBehaviour
{
    bool hasScaledPanel;
    private void Update()
    {
        if (!hasScaledPanel)
        {
            if (GetRoomSize.Size != null)
            {
                if (GetRoomSize.Size.z > 1.5f)
                {
                    transform.localScale = new Vector3(0.1f, 1.5f, GetRoomSize.Size.z);
                }
                else
                {
                    transform.localScale = new Vector3(0.1f, GetRoomSize.Size.z, GetRoomSize.Size.z);
                }
                hasScaledPanel = true;
            }
        }
    }
}
