﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRandomizer : MonoBehaviour
{
    enum RandomizerState { NotRandomized, DistanceCheck, Randomized }
    static RandomizerState currentState = RandomizerState.NotRandomized;
    [SerializeField] GameObject buttonWall;
    List<Transform> placedObjects = new List<Transform>();
    private void Update()
    {
        if (currentState == RandomizerState.NotRandomized)
        {
            if (GetRoomSize.Size != null)
            {
                foreach (Transform a in transform)
                {
                    Randomizer(a);
                    placedObjects.Add(a);
                }
                currentState = RandomizerState.DistanceCheck;
            }
        }
        bool passedDistanceCheck = true;
        if (currentState == RandomizerState.DistanceCheck)
        {
            // reset the distance checking flag
            passedDistanceCheck = true;
            foreach (Transform b in placedObjects)
            {
                for (int i = 0; i < placedObjects.Count; i++)
                {
                    if (Vector3.Distance(placedObjects[i].position, b.position) < 0.2f)
                    {
                        if (placedObjects[i].gameObject != b.gameObject)
                        {
                            passedDistanceCheck = false;
                            Randomizer(b);
                        }
                    }
                }
            }
            if (passedDistanceCheck)
            {
                currentState = RandomizerState.Randomized;
            }
        }
    }
    void Randomizer(Transform button)
    {
        Vector3 bounds = (buttonWall.transform.localScale * 0.7f) / 2f;
        button.localPosition = new Vector3(0, Random.Range(-bounds.y, bounds.y), Random.Range(-bounds.z, bounds.z));
    }
    public static void ReRandomize()
    {
        currentState = RandomizerState.NotRandomized;
    }
}