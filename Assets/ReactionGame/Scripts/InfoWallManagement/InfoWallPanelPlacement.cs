﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoWallPanelPlacement : MonoBehaviour
{
    bool hasPlacedPanel;
    private void Update()
    {
        if (!hasPlacedPanel)
        {
            if (GetRoomSize.Size != null)
            {
                if (GetRoomSize.Size.z < 1.5f)
                {
                    transform.position = new Vector3(0f, 1.25f, 1.5f);
                }
                else
                {
                    transform.position = new Vector3(0f, 1.25f, -GetRoomSize.Size.z / 2f);
                }
                hasPlacedPanel = true;
            }
        }
    }
}