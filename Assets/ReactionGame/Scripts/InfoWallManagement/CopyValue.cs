﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyValue : MonoBehaviour
{
    [SerializeField] Text scoreText;
    private void OnEnable()
    {
        GetComponent<Text>().text = scoreText.text;
    }
}
