﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables.ArtificialBased;

public class GameStarter : MonoBehaviour
{
    [SerializeField] GameObject timerCanvas;
    [SerializeField] GameObject infoCanvas;
    float waitTime = 1.0f;
    private void OnEnable()
    {
        waitTime = 1.0f;
    }
    private void Update()
    {
        if (GetComponentInChildren<VRTK_ArtificialPusher>().GetNormalizedValue() > 0.8f && waitTime <= 0)
        {
            timerCanvas.SetActive(true);
            infoCanvas.SetActive(false);
            GameStateManager.StartGame();
            ButtonRandomizer.ReRandomize();
        }
        waitTime -= Time.deltaTime;
    }
}
