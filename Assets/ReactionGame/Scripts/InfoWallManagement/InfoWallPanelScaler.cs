﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoWallPanelScaler : MonoBehaviour
{
    bool hasScaledPanel;
    private void Update()
    {
        if (!hasScaledPanel)
        {
            if (GetRoomSize.Size != null)
            {
                transform.localScale = new Vector3(GetRoomSize.Size.x / 2f, GetRoomSize.Size.x / 2f, 0.01f);
                hasScaledPanel = true;
            }
        }
    }
}
